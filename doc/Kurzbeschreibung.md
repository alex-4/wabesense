# Kurzbeschreibung WABEsense

## Deutsch

Das vom BAFU finanzierte Projekt WABEsense (https://hsr-iet.gitlab.io/wabesense/blog/) wird alpine Wasserquellen sensorisieren und ihre Wasserproduktion überwachen. Es handelt sich um ein Gemeinschaftsprojekt mit der Uli Lippuner AG (https://www.ulippuner.ch/) und dem Institut für Mikroelektronik und Embedded Systems.

Während des Projekts werden wir eine offene Hardwarelösung entwickeln, die in das Budget der Wasserversorgungs der Gemeinde passt (insbesondere ein Datenlogger), sowie die IT-Infrastruktur für die offene gemeinsame Nutzung der Daten (zu finden unter https://opendata.swiss).Die Wasserproduktionsdaten werden es den Gemeinden ermöglichen, ihre Generelle Wasserversorgungsplanung und ihre Nachhaltigkeit zu verbessern. Die Daten können auch von Forschern verwendet werden, um das Verhalten alpiner Wasserquellen und des Grundwasserspiegels zu modellieren und zu verstehen.

Warum offene Hardware? Mit dem Aufkommen von Open Source und Open Hardware (https://www.oshwa.org/sharing-best-practices/best-practices-der-open-source-hardware-1-0/) werden geschlossene technologische Lösungen immer uninteressanter.
Open Technologien haben für viele Unternehmen klare Vorteile: 1) sie verlagern den Schwerpunkt auf Dienstleistungen und immaterielle Güter, 2) sie schaffen Vertrauen auf der Grundlage von Transparenz und Beteiligung.
In 1) ist das Produkt jetzt das, was man mit der Technologie macht, und nicht die Technologie selbst. Man dominiert den Markt, wenn man das Beste aus der Technologie macht. Stellen Sie sich ein Werkzeug in der Hand eines erfahrenen Handwerkers vor: Jeder weiß, wie das Werkzeug funktioniert, jeder könnte das Werkzeug bauen, aber nur der erfahrene Handwerker kann damit zaubern.
In 2) können die Kunden direkt an der Entwicklung der Technologie teilnehmen, sie werden Partner. Sie vertrauen der Technologie, weil sie sie **verstehen** und **beeinflussen** können. Verbesserungs- und Entwicklungskosten können von vielen Parteien geteilt werden, die Technologie wird zu einem Gemeinschaftsgut.
Wasserversorgungen, die die WABEsense-Technologie verwenden, sind nicht eingefangen. Sie können in völliger Freiheit reparieren, verbessern und hacken.

WABEsense wurde als ein wirklich offenes Projekt konzipiert, das in einem kommerziellen Umfeld umgesetzt wird, und es wird einen Präzedenzfall für die Auswirkungen offener Projekte schaffen.

English
-------
The BAFU funded project WABEsense (https://hsr-iet.gitlab.io/wabesense/blog/) will sensorize alpine water springs and monitor their water production. This is a joint effort with Uli Lippuner AG (https://www.ulippuner.ch/) and the Institut für Mikroelektronik und Embedded Systems.

During the project we will develop an open hardware solution that fits in the budget of municipality water utilities (in particular a data logger), and the IT infrastructure to share the data openly (to be located at https://opendata.swiss). The water production data will allow municipalities to enhance their Generelle Wasserversorgungsplanung and their sustainability; and it can also be used by researchers to model the behavior of alpine water springs and water table. 

Why open hardware? With the advent of open source and open hardware (https://www.oshwa.org/sharing-best-practices/best-practices-der-open-source-hardware-1-0/), closed technological solutions are becoming less interesting.
Open technologies have clear advantages for many business: 1) they shift the focus to services and intangibles, 2) they build trust based on transparency and participation.
In 1) the product is now what you do with the technology and not the technology itself. You dominate the market when you do the best of the technology. Think of a tool in the hand of an expert craftsman: everybody knows how the tool works, everybody could build the tool, but only the expert craftsman can do magic with it.
In 2) clients can directly participate in the development of the technology, they become partners. They trust the technology because they **can** understand it, and influence it. Enhancement and development costs can be shared among many parties, the technology becomes a common good.
Municipal water utilities using WABEsense technology, are not trapped. They can repair, enhance, and hack with total freedom.

WABEsense was designed to be a truly open project implemented in an commercial environment and it will set a precedent on the impact of open projects.
