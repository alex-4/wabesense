# WABEsense

The sensorization of existing alpine spring capture boxes and the generation of year-round discharge charts allow sustainable water management. Custom build boxes will be sensorized and their discharge curve determined using CFD